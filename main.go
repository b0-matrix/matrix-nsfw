package main

import (
	"bytes"
	"flag"
	"fmt"
	"image"
	"io"
	"log"
	"strings"

	"encoding/json"
	"image/draw"
	"image/gif"
	"image/png"
	"net/http"
	"net/url"

	"github.com/matrix-org/gomatrix"
	"gocv.io/x/gocv"
)

func main() {
	var serverAddr, userID, token string

	flag.StringVar(&serverAddr, "addr", "https://matrix.org", "Homeserver address")
	flag.StringVar(&userID, "user", "@example:matrix.org", "User ID")
	flag.StringVar(&token, "token", "MDAefhiuwehfuiwe", "User Token")

	flag.Parse()

	log.Println("Server:", serverAddr)
	log.Println("User:", userID)
	log.Println("Token:", token)

	cli, _ := gomatrix.NewClient(serverAddr, userID, token)

	syncer := cli.Syncer.(*gomatrix.DefaultSyncer)

	syncer.OnEventType("m.room.member", func(ev *gomatrix.Event) {
		if ev.Content["membership"].(string) == "invite" {
			log.Println("Joining ", ev.RoomID)
			_, err := cli.JoinRoom(ev.RoomID, "", "")
			if err != nil {
				log.Println("Joining failed:", err)
			}
		}
	})

	syncer.OnEventType("m.room.message", func(ev *gomatrix.Event) {
		if ev.Sender == userID {
			return
		}

		if ev.Content["msgtype"] == "m.image" {
			var filename string
			if f, ok := ev.Content["filename"]; ok {
				filename = f.(string)
			} else if b, ok := ev.Content["body"]; ok {
				filename = b.(string)
			}

			mxc := ev.Content["url"].(string)

			uri, _ := url.Parse(mxc)
			downloadAddr := serverAddr + "/_matrix/media/r0/download/" + uri.Host + uri.Path
			resp, err := http.Get(downloadAddr)
			if err != nil {
				return
			}
			defer resp.Body.Close()

			buf := new(bytes.Buffer)

			if strings.HasSuffix(filename, ".gif") {
				g, err := gif.DecodeAll(resp.Body)
				if err != nil {
					panic(err)
				}

				overpaintImage := image.NewRGBA(image.Rect(0, 0, g.Config.Width, g.Config.Height))
				draw.Draw(overpaintImage, overpaintImage.Bounds(), g.Image[0], image.ZP, draw.Src)

				err = png.Encode(buf, overpaintImage)
				if err != nil {
					log.Println("Error in decoding GIF:", err)
				}
			} else {
				_, err = io.Copy(buf, resp.Body)
				if err != nil {
					return
				}
			}

			result, err := predict(buf.Bytes())
			if err != nil {
				log.Println("Error when predicting image:", err)
				return
			}

			rsp, _ := json.Marshal(struct {
				Result bool    `json:"result"`
				Score  float32 `json:"score"`
			}{
				Result: result >= 0.2,
				Score:  result,
			})

			type InReplyTo struct {
				EventID string `json:"event_id"`
			}
			type RelatesTo struct {
				InReplyTo InReplyTo `json:"m.in_reply_to"`
			}
			cli.SendMessageEvent(ev.RoomID, "m.room.message",
				struct {
					MsgType   string    `json:"msgtype"`
					Body      string    `json:"body"`
					RelatesTo RelatesTo `json:"m.relates_to"`
				}{
					MsgType:   "m.text",
					Body:      string(rsp),
					RelatesTo: RelatesTo{InReplyTo: InReplyTo{EventID: ev.ID}},
				})
		}
	})

	if err := cli.Sync(); err != nil {
		fmt.Println("Program ended with ", err)
	}
}

func predict(rawImage []byte) (float32, error) {
	im, err := gocv.IMDecode(rawImage, gocv.IMReadColor)
	if err != nil {
		return 0, err
	}
	blob := gocv.BlobFromImage(im, 1, image.Pt(224, 224), gocv.NewScalar(104, 117, 123, 0), false, false)

	net := gocv.ReadNetFromCaffe("model/model.prototxt", "model/model.caffemodel")
	net.SetInput(blob, "")
	preds := net.Forward("")

	return preds.GetFloatAt(0, 1), nil
}
