# matrix-nsfw

An API for detecting NSFW images, running on Matrix protocol.

# Target Prerequisite

* Golang 1.11
* OpenCV 3.4.3

# Install Go Dependencies

```bash
go get -d ./...
```

# Install OpenCV

## Debian-derived

Just compile from source.

Please visit https://milq.github.io/install-opencv-ubuntu-debian/ if you don't know how to install OpenCV 3.4.3 from source.

## Arch Linux-derived

opencv from Arch Linux repository should work fine.

## Fedora

For Fedora Rawhide, opencv-devel should work fine.

For Fedora 29, you need to compile OpenCV from source.

## Compiling

```bash
cd $GOPATH/src/gocv.io/x/gocv
make install
```